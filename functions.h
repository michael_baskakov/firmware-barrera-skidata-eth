#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "peripheral.h"
#include <string.h>
#include <avr/wdt.h>
#include "udpmanager.h"

#define HSU 0


typedef struct Inp{
    int input;
    int pin;
    int val;
}Inp;

typedef struct Eepvar{
    char id[26];
    char mask[3];
}Eepvar;





void procmsg(char *c);
void sendsuc(int input, int val,char *msjid);
void check_inputs();
void sendsta(char *msjid);
void load_eep();
void apertura(unsigned long tout,unsigned long tclose,char *msjid);
void cierre_barrera(char *msjid);
void abrir_barrera(char *msjid);

#endif
