import qbs
import qbs.FileInfo

CppApplication {
    consoleApplication: true
    //type: hex
    files: [
        "functions.cpp",
        "functions.h",
        "mainavr.cpp",
        "peripheral.h",
        "udpmanager.cpp",
        "udpmanager.h",
    ]

    Group {     // Properties for the produced executable
        fileTagsFilter: product.type
        qbs.install: true
    }

    qbs.architecture: "avr"

    cpp.defines:        [   'F_CPU=16000000L','USB_VID=0x2341','USB_PID=0x8036','ARDUINO=10801','ARDUINO_ARCH_AVR',
                            'ARDUINO_AVR_LEONARDO','USB_VID=0x2341','USB_PID=0x8036',
                            'USB_MANUFACTURER="Unknown"','USB_PRODUCT="Arduino Leonardo"']

    cpp.includePaths:   [   "/home/mbv/arduino-1.8.1/hardware/arduino/avr/cores/arduino/",
                            "/home/mbv/arduino-1.8.1/hardware/arduino/avr/variants/leonardo/",
                            "/home/mbv/arduino-1.8.1/hardware/arduino/avr/libraries/EEPROM/src/",
                            "/home/mbv/arduino-1.8.1/hardware/arduino/avr/libraries/SPI/src/",
                            "/home/mbv/arduino-1.8.1/libraries/Ethernet/src/"]

    cpp.cxxFlags:       [   "-mmcu=atmega32u4","-std=gnu++11","-c","-g","-Os",
                            "-fno-exceptions","-fpermissive","-ffunction-sections","-fdata-sections","-fno-threadsafe-statics",
                            "-flto","-x","c++","-CC"]

    //-E
//    cpp.cxxFlags:       [   "-mmcu=atmega32u4","-std=gnu++11","-fno-exceptions",
//                                   "-ffunction-sections","-fdata-sections",
//                                   "-fno-threadsafe-statics","-CC","-x","c++"]

    cpp.linkerFlags:    [   "-Wall","-Wextra","-Wl,--gc-sections",
                            "-Os","-g","-flto","-fuse-linker-plugin",
                            "-mmcu=atmega32u4"]

    cpp.staticLibraries:    ['m','EthernetSpi','coreavr']

    cpp.libraryPaths:   [   "/media/DOCS/Project/Safecard/ControladorBarrera_1.8.1_ETH_SKIDATA"]

    Rule {
            id: hex
            inputs: ["application"]

            Artifact {
                fileTags: ["hex"]
                filePath: input.fileName + ".hex"
            }

            prepare: {
                var objCopyPath = "avr-objcopy";
                var args = ["-O", "ihex", input.filePath, output.filePath];
                var cmd = new Command(objCopyPath, args);

                cmd.description = "converting to hex: " + FileInfo.fileName(input.filePath);
                //cmd.highlight = "linker";

                return cmd;
            }
        }

}

