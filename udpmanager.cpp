#include "udpmanager.h"

Udpmanager* Udpmanager::inst = 0;

Udpmanager::Udpmanager()
{
    Udp.begin(23000);
    host = IPAddress(10,0,32,2);

}



Udpmanager* Udpmanager::getinst(){
    if(!inst){
        inst = new Udpmanager;
    }
    return inst;
}

void Udpmanager::send_packet(char *c){
    Udp.beginPacket(host, 23001);
    Udp.write(c);
    Udp.endPacket();
}

bool Udpmanager::receive_packet(char *c){
    int packetSize = Udp.parsePacket();
    if(packetSize){
        Udp.read(c,100);
        return true;
    }else{
        return false;
    }
}
