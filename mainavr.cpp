#include <SPI.h>
#include <Arduino.h>
#include <EthernetUdp.h>
#include <Ethernet.h>
#include <EthernetClient.h>
#include <Dns.h>
#include <Dhcp.h>
#include <EthernetServer.h>
#include <avr/wdt.h>
#include "peripheral.h"
#include "functions.h"

#include <EEPROM.h>
#include "udpmanager.h"

Udpmanager *udpm;

void setup() {

    SPI.begin();
    wdt_disable();
    EEPROM.begin();
    Serial1.begin(115200);
    pinMode(RELE1,OUTPUT);
    pinMode(RELE2,OUTPUT);
    pinMode(6,INPUT_PULLUP);
    pinMode(9,INPUT_PULLUP);
    pinMode(11,INPUT_PULLUP);
    pinMode(12,INPUT_PULLUP);
    pinMode(13,INPUT_PULLUP);
    pinMode(2,INPUT_PULLUP);
    pinMode(3,OUTPUT);
    pinMode(4,INPUT_PULLUP);
    pinMode(5,INPUT_PULLUP);

    byte mac[] = { 0x00, 0x08, 0xDC, 0x32, 0x02, 0x00 };
    IPAddress ip(10, 0, 32, 20);
    IPAddress gw(10,0,32,1);
    IPAddress dnsaddr(10,0,32,1);
    IPAddress nm(255,255,255,0);
    Ethernet.begin(mac,ip,dnsaddr,gw,nm);
    udpm = Udpmanager::getinst();
    load_eep();

    wdt_enable(WDTO_8S);


}

void loop() {
    static char udpbuff[100];
    wdt_reset();
    if(udpm->receive_packet(udpbuff))
        procmsg(udpbuff);
    check_inputs();
}
