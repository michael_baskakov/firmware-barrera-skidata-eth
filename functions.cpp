
#include <Arduino.h>
#include <EEPROM.h>
#include "functions.h"

Udpmanager *udpmp;


static Inp inp[] =  {{0,6,1},
                    {1,9,1},
                    {2,11,1},
                    {3,12,1},
                    {4,13,1},
                    {5,4,1},
                    {6,5,1},
                    {7,2,1},
                    };

static int inpsize = sizeof(inp)/sizeof(Inp);

Eepvar eepvar;
char msjid[26];

void delay_ms(uint32_t ms){
    uint32_t tmp = millis();
    while((millis() -  tmp) <= ms);
}


void cierre_barrera(char *msjid){
    if(digitalRead(BCLOSE))
        return;
    digitalWrite(RELE2,1);
    while(digitalRead(BOPEN)){
        check_inputs();
        //delay_ms(100);
    }
    while(!digitalRead(BCLOSE)){
        check_inputs();
        //delay_ms(100);
    }
    digitalWrite(RELE2,0);
    sendsuc(1,1,msjid); //input 1 BCLOSE 1:cierre
}

void abrir_barrera(char *msjid){
    digitalWrite(RELE1,1);
    while(digitalRead(BCLOSE)){
        delay_ms(100);
    }
    while(!digitalRead(BOPEN)){
        delay_ms(100);
    }
    digitalWrite(RELE1,0);
    sendsuc(0,1,msjid); //input 0 BOPEN 1:abertura
}



void apertura(unsigned long tout,unsigned long tclose,char *msjid){
    //RELE1 open, RELE2 close
    unsigned long tmptime,currentime;
    if(digitalRead(BOPEN))
        return;
    abrir_barrera(msjid);

    tmptime = millis();
    while(digitalRead(LOOP)){
        delay_ms(200);
        currentime = millis();
        if(currentime - tmptime >= tout){
            cierre_barrera(msjid);
            return;
        }
    }

    while(!digitalRead(LOOP)){
        delay_ms(200);
    }
    sendsuc(2,1,msjid); //input 2 LOOP 1:paso por loop
    delay_ms(tclose);
    cierre_barrera(msjid);
}

void procmsg(char *c){
    char *tok;

    int val,ms;

    tok = strtok(c,",\r");

    if(tok != 0){
       if(strcmp(tok,eepvar.id) != 0)
           return;
    }else{
       return;
    }

    tok = strtok(0,",\r");
    if(!tok)
       return;

    //agrega msjid
    strcpy(msjid,tok);

    tok = strtok(0,",\r");
    if(!tok)
       return;


    //compatibilidad generico
    if(strcmp(tok,"1") == 0){
        digitalWrite(RELE1,1);
        delay_ms(500);
        digitalWrite(RELE1,0);
        return;
    }
    if(strcmp(tok,"2") == 0){
        digitalWrite(RELE2,1);
        delay_ms(500);
        digitalWrite(RELE2,0);
        return;
    }

    if(strcmp(tok,"ACT") == 0){
        tok = strtok(0,",\r");
        if(!tok)
           return;

        if(strcmp(tok,"1") == 0){
            tok = strtok(0,",\r");
            if(!tok)
               return;
            val = atoi(tok);
            digitalWrite(RELE1,val);
            return;
        }else if(strcmp(tok,"2") == 0){
            tok = strtok(0,",\r");
            if(!tok)
               return;
            val = atoi(tok);
            digitalWrite(RELE2,val);
            return;
        }else if(strcmp(tok,"3") == 0){
            tok = strtok(0,",\r");
            if(!tok)
               return;
            val = atoi(tok);
            digitalWrite(OUTDIG,val);
            return;
        }
    }
    if(strcmp(tok,"PLS") == 0){
        tok = strtok(0,",\r");
        if(!tok)
           return;
        if(strcmp(tok,"1") == 0){
            tok = strtok(0,",\r");
            if(!tok)
               return;
            val = atoi(tok);
            tok = strtok(0,",\r");
            if(!tok)
               return;
            ms = atoi(tok);
            digitalWrite(RELE1,val);
            delay_ms(ms);
            digitalWrite(RELE1,!val);
            return;
        }else if(strcmp(tok,"2") == 0){
            tok = strtok(0,",\r");
            if(!tok)
               return;
            val = atoi(tok);
            tok = strtok(0,",\r");
            if(!tok)
               return;
            ms = atoi(tok);
            digitalWrite(RELE2,val);
            delay_ms(ms);
            digitalWrite(RELE2,!val);
            return;
        }else if(strcmp(tok,"3") == 0){
            tok = strtok(0,",\r");
            if(!tok)
               return;
            val = atoi(tok);
            tok = strtok(0,",\r");
            if(!tok)
               return;
            ms = atoi(tok);
            digitalWrite(OUTDIG,val);
            delay_ms(ms);
            digitalWrite(OUTDIG,!val);
            return;
        }
    }

    if(strcmp(tok,"HSU") == 0){
        tok = strtok(0,",\r");
        if(!tok)
           return;
        strcpy(eepvar.mask,tok);
        long mask = strtol(eepvar.mask,NULL,16);
        if(mask >= 0 && mask <=255)
            EEPROM.put(0,eepvar);
        return;
        }

    if(strcmp(tok,"STA") == 0){
        sendsta(msjid);
        return;
    }

    if(strcmp(tok,"IDC") == 0){
        tok = strtok(0,",\r");
        if(!tok)
           return;
        strcpy(eepvar.id,tok);
        EEPROM.put(0,eepvar);
        return;
    }

    if(strcmp(tok,"ABT") == 0){
        tok = strtok(0,",\r");
        if(!tok)
           return;
        unsigned long tout = atol(tok);
        tok = strtok(0,",\r");
        if(!tok)
           return;
        unsigned long tclose = atol(tok);
        wdt_disable();
        apertura(tout,tclose,msjid);
        wdt_enable(WDTO_8S);
        return;
    }

    if(strcmp(tok,"CBR") == 0){
        cierre_barrera(msjid);
        return;
    }

    if(strcmp(tok,"ABP") == 0){
        if(digitalRead(BOPEN))
            return;
        abrir_barrera(msjid);
        return;
    }

    sendsta(msjid);


}

void sendsta(char *msjid){
    char c[255];
    sprintf(c,"%s,%s,INVALID\r",eepvar.id,msjid);
    udpmp->send_packet(c);

}

void sendsuc(int input, int val,char *msjid){
    char c[50];
    int hsu = strtol(eepvar.mask,NULL,16);

    if(bitRead(hsu,input)){
        if(input == 7 && val == 1){
            sprintf(c,"%s,0,ABT,%d,%d",eepvar.id,15000,0);
            procmsg(c);
        }
        sprintf(c,"%s,%s,SUC,%d,%d\r",eepvar.id,msjid,input,val);
        udpmp->send_packet(c);
    }
}

void load_eep(){
    char c[50];
    EEPROM.get(0,eepvar);
    if((uint8_t)eepvar.id[0] == 255){
        strcpy(eepvar.mask,"FF");
        strcpy(eepvar.id,"1234");
        EEPROM.put(0,eepvar);
    }
    delay_ms(5000);
    sprintf(c,"Controlador - %s - En Linea\r",eepvar.id);
    udpmp = Udpmanager::getinst();
    udpmp->send_packet(c);


}


void check_inputs(){
    for(int j = 3;j < inpsize;j++){
        if(inp[j].val != digitalRead(inp[j].pin)){
            inp[j].val = digitalRead(inp[j].pin);
            //segundo sensor de loop o OJO NA
            if(j == 6){
                if(inp[6].val == 1)
                    sendsuc(6,1,msjid); //input 2 LOOP 1:paso por loop
            }else if(j == 7){
                if(inp[7].val == 1)
                    sendsuc(7,1,"0"); //control remoto
            }else{
                sendsuc(inp[j].input,inp[j].val,msjid);
            }
            delay_ms(100);
        }
    }
}
