#ifndef UDPMANAGER_H
#define UDPMANAGER_H

#include <EthernetUdp.h>

class Udpmanager
{
public:
    static Udpmanager* getinst();
    void send_packet(char *);
    bool receive_packet(char *);

private:
    EthernetUDP Udp;
    IPAddress host;
    static Udpmanager* inst;
    Udpmanager();
};



#endif // UDPMANAGER_H
